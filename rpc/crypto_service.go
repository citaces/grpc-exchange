package rpcservice

import (
	"context"

	"gitlab.com/citaces/grpc-exchange/internal/models"
	cservice "gitlab.com/citaces/grpc-exchange/internal/modules/crypto/service"
)

type CryptoServiceRPC struct {
	service cservice.Crypter
}

func NewCryptoRPCService(service cservice.Crypter) *CryptoServiceRPC {
	return &CryptoServiceRPC{service: service}
}

func (c *CryptoServiceRPC) Update(in struct{}, out *models.Coins) error {
	c.service.Update(context.Background(), *out)
	return nil
}

func (c *CryptoServiceRPC) Cost(in struct{}, out *models.CostOut) error {
	*out = c.service.Cost(context.Background())
	return nil
}

func (c *CryptoServiceRPC) History(in struct{}, out *models.HistoryOut) error {
	*out = c.service.History(context.Background())
	return nil
}

func (c *CryptoServiceRPC) Max(in struct{}, out *models.MaxOut) error {
	*out = c.service.Max(context.Background())
	return nil
}

func (c *CryptoServiceRPC) Min(in struct{}, out *models.MinOut) error {
	*out = c.service.Min(context.Background())
	return nil
}

func (c *CryptoServiceRPC) Avg(in struct{}, out *models.AvgOut) error {
	*out = c.service.Avg(context.Background())
	return nil
}
