package storage

import (
	"log"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func RabbitMQ(body string) error {

	conn, err := amqp.Dial("amqp://guest:guest@rabbit1:5672/")
	failOnError(err, `rabbitmq: dial err`)

	ch, err := conn.Channel()
	failOnError(err, `rabbitmq: channel err`)

	log.Println(`successfully connected to RabbitMQ instance`)

	q, err := ch.QueueDeclare(
		`Crypto_upd`,
		false,
		false,
		false,
		false,
		nil,
	)

	failOnError(err, `rabbitmq: queue declare err`)

	err = ch.Publish(
		``,
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(body),
		})
	failOnError(err, `rabbitmq: publish err`)

	log.Println(`successfully published message to queue`)
	return nil
}
