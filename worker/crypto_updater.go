package worker

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/citaces/grpc-exchange/internal/modules"

	"gitlab.com/citaces/grpc-exchange/internal/models"
)

type CryptoUpdater struct {
	service modules.Services
}

func NewCryptoUpdater(service *modules.Services) *CryptoUpdater {
	return &CryptoUpdater{service: *service}
}

func (c *CryptoUpdater) Run() {
	ticker := time.NewTicker(15 * time.Second)
	done := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				response, err := http.Post(
					"https://api.exmo.com/v1.1/ticker",
					"application/x-www-form-urlencoded",
					bytes.NewBuffer([]byte{}),
				)
				if err != nil {
					log.Fatal(err)
				}
				m := make(models.Coins)
				err = json.NewDecoder(response.Body).Decode(&m)
				if err != nil {
					log.Fatal(err)
				}
				c.service.Crypto.Update(context.Background(), m)
			case <-done:
				return
			}
		}
	}()
}
