package storages

import (
	"gitlab.com/citaces/grpc-exchange/internal/db/adapter"
	"gitlab.com/citaces/grpc-exchange/internal/infrastructure/cache"
	"gitlab.com/citaces/grpc-exchange/internal/modules/crypto/storage"
)

type Storages struct {
	Crypto storage.Crypter
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Crypto: storage.NewCryptoStorage(sqlAdapter, cache),
	}
}
