package crypto

import (
	"gitlab.com/citaces/grpc-exchange/internal/infrastructure/component"
	cservice "gitlab.com/citaces/grpc-exchange/internal/modules/crypto/service"
	"gitlab.com/citaces/grpc-exchange/internal/storages"
)

type Services struct {
	Crypto cservice.Crypter
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	cryptoService := cservice.NewCryptoService(storages.Crypto, components.Logger)
	return &Services{
		Crypto: cryptoService,
	}
}
