package models

type CostOut struct {
	Coins     []Coin `json:"coins"`
	ErrorCode int    `json:"error_code"`
}
type HistoryOut struct {
	Coins     []Coin `json:"coins"`
	ErrorCode int    `json:"error_code"`
}
type MaxOut struct {
	Coins     []Coin `json:"coins"`
	ErrorCode int    `json:"error_code"`
}
type MinOut struct {
	Coins     []Coin `json:"coins"`
	ErrorCode int    `json:"error_code"`
}
type AvgOut struct {
	Coins     []Coin `json:"coins"`
	ErrorCode int    `json:"error_code"`
}
