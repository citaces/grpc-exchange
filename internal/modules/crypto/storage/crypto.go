package storage

import (
	"context"
	"fmt"
	"log"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/citaces/grpc-exchange/internal/infrastructure/errors"

	"gitlab.com/citaces/grpc-exchange/internal/infrastructure/db/scanner"

	"gitlab.com/citaces/grpc-exchange/internal/db/adapter"
	"gitlab.com/citaces/grpc-exchange/internal/infrastructure/cache"
	coinModels "gitlab.com/citaces/grpc-exchange/internal/models"
)

var maxData = map[string]float64{}
var minData = map[string]float64{}

type CryptoStorage struct {
	adapter *adapter.SQLAdapter
	cache   cache.Cache
}

func NewCryptoStorage(adapter *adapter.SQLAdapter, cache cache.Cache) *CryptoStorage {
	return &CryptoStorage{adapter: adapter, cache: cache}
}

func (c *CryptoStorage) Update(ctx context.Context, models coinModels.Coins) {

	var maxDto coinModels.CryptoMaxDTO
	var minDto coinModels.CryptoMinDTO

	var err error

	var listMax []coinModels.CryptoMaxDTO
	var listMin []coinModels.CryptoMinDTO

	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from table %v`, maxDto)
	}

	err = c.adapter.List(ctx, &listMin, minDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from table %v`, minDto)
	}

	for k, v := range models {
		i := 0
		var bufferMax coinModels.CryptoMaxDTO
		var bufferMin coinModels.CryptoMinDTO

		if len(listMax) < 1 || len(listMin) < 1 {

			bufferMax.SetName(k).SetAvg(v.Avg).SetUpdated(int(v.Updated)).SetBuyPrice(v.BuyPrice).SetHigh(v.High).SetLastTrade(v.LastTrade).SetLow(v.Low).SetSellPrice(v.SellPrice).SetVol(v.Vol).SetVolCurr(v.VolCurr)
			bufferMin.SetName(k).SetAvg(v.Avg).SetUpdated(int(v.Updated)).SetBuyPrice(v.BuyPrice).SetHigh(v.High).SetLastTrade(v.LastTrade).SetLow(v.Low).SetSellPrice(v.SellPrice).SetVol(v.Vol).SetVolCurr(v.VolCurr)

			err = c.adapter.Create(ctx, &bufferMax)
			if err != nil {
				log.Fatalf(`error when trying to create data at table %v`, maxDto)
			}
			err = c.adapter.Create(ctx, &bufferMin)
			if err != nil {
				log.Fatalf(`error when trying to create data at table %v`, minDto)
			}
		} else {
			if v.High > listMax[i].GetHigh() {
				bufferMax.SetName(k).SetAvg(v.Avg).SetUpdated(int(v.Updated)).SetBuyPrice(v.BuyPrice).SetHigh(v.High).SetLastTrade(v.LastTrade).SetLow(v.Low).SetSellPrice(v.SellPrice).SetVol(v.Vol).SetVolCurr(v.VolCurr)
				err = c.adapter.Update(ctx, &bufferMax, adapter.Condition{
					Equal: sq.Eq{"name": k},
				}, scanner.Update)
				if err != nil {
					log.Fatalf(`error when trying to update data at table %v`, maxDto)
				}

				if val, ok := maxData[k]; !ok {
					maxData[k] = bufferMax.GetHigh()
					continue
				} else if ok && bufferMax.GetHigh()-val >= 100 {

					err = RabbitMQ(fmt.Sprintf("минимальная цена для %s: была %f., а стала =  %f.", k, val, bufferMax.GetHigh()))
					if err != nil {
						log.Fatal(`error when trying to push notify max value to service`)
					}
					maxData[k] = bufferMax.GetHigh()

				}

			}
			if v.Low < listMin[i].GetLow() {
				bufferMin.SetName(k).SetAvg(v.Avg).SetUpdated(int(v.Updated)).SetBuyPrice(v.BuyPrice).SetHigh(v.High).SetLastTrade(v.LastTrade).SetLow(v.Low).SetSellPrice(v.SellPrice).SetVol(v.Vol).SetVolCurr(v.VolCurr)
				err = c.adapter.Update(ctx, &bufferMin, adapter.Condition{
					Equal: sq.Eq{"name": k},
				}, scanner.Update)
				if err != nil {
					log.Fatalf(`error when trying to update data at table %v`, minDto)
				}

				if val, ok := minData[k]; !ok {
					minData[k] = bufferMin.GetLow()
					continue
				} else if ok && val-bufferMin.GetLow() >= 100 {

					err = RabbitMQ(fmt.Sprintf("минимальная цена для %s: была %f., а стала =  %f.", k, val, bufferMin.GetLow()))
					if err != nil {
						log.Fatal(`error when trying to push notify min value to service`)
					}
					minData[k] = bufferMin.GetLow()
				}

			}
		}
		i++
	}
}

func (c *CryptoStorage) Cost(ctx context.Context) coinModels.CostOut {
	var maxDto coinModels.CryptoMaxDTO
	var err error

	var listMax []coinModels.CryptoMaxDTO
	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Cost and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return coinModels.CostOut{
			ErrorCode: errors.CoinStorageRetrieveCostErr,
		}
	}
	var result []coinModels.Coin
	for _, e := range listMax {
		result = append(result, coinModels.Coin{
			ID:       int64(e.GetID()),
			Name:     e.GetName(),
			BuyPrice: e.GetBuyPrice(),
		})
	}
	return coinModels.CostOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (c *CryptoStorage) History(ctx context.Context) coinModels.HistoryOut {
	var maxDto coinModels.CryptoMaxDTO
	var err error

	var listMax []coinModels.CryptoMaxDTO
	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method History and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return coinModels.HistoryOut{
			ErrorCode: errors.CoinStorageRetrieveHistoryErr,
		}
	}
	var result []coinModels.Coin
	for _, e := range listMax {
		result = append(result, coinModels.Coin{
			ID:        int64(e.GetID()),
			Name:      e.GetName(),
			LastTrade: e.GetLastTrade(),
		})
	}

	return coinModels.HistoryOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (c *CryptoStorage) Max(ctx context.Context) coinModels.MaxOut {
	var maxDto coinModels.CryptoMaxDTO
	var err error

	var listMax []coinModels.CryptoMaxDTO
	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Max and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return coinModels.MaxOut{
			ErrorCode: errors.CoinStorageRetrieveMaxErr,
		}
	}
	var result []coinModels.Coin
	for _, e := range listMax {
		result = append(result, coinModels.Coin{
			ID:   int64(e.GetID()),
			Name: e.GetName(),
			High: e.GetHigh(),
		})
	}

	return coinModels.MaxOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (c *CryptoStorage) Min(ctx context.Context) coinModels.MinOut {
	var minDto coinModels.CryptoMinDTO
	var err error

	var listMin []coinModels.CryptoMinDTO
	err = c.adapter.List(ctx, &listMin, minDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Min and table %v`, minDto)
	}
	if len(listMin) == 0 {
		return coinModels.MinOut{
			ErrorCode: errors.CoinStorageRetrieveMinErr,
		}
	}
	var result []coinModels.Coin
	for _, e := range listMin {
		result = append(result, coinModels.Coin{
			ID:   int64(e.GetID()),
			Name: e.GetName(),
			Low:  e.GetLow(),
		})
	}

	return coinModels.MinOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (c *CryptoStorage) Avg(ctx context.Context) coinModels.AvgOut {
	var maxDto coinModels.CryptoMaxDTO
	var err error

	var listMax []coinModels.CryptoMaxDTO
	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Avg and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return coinModels.AvgOut{
			ErrorCode: errors.CoinStorageRetrieveAvgErr,
		}
	}
	var result []coinModels.Coin
	for _, e := range listMax {
		result = append(result, coinModels.Coin{
			ID:   int64(e.GetID()),
			Name: e.GetName(),
			Avg:  e.GetAvg(),
		})
	}

	return coinModels.AvgOut{
		Coins:     result,
		ErrorCode: 0,
	}
}
